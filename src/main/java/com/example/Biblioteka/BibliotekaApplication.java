package com.example.Biblioteka;

import com.example.Biblioteka.book.Book;
import com.example.Biblioteka.book.BookController;
import com.example.Biblioteka.user.User;
import com.example.Biblioteka.user.UserController;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import java.util.List;

@SpringBootApplication
public class BibliotekaApplication implements CommandLineRunner {

    private BookController bookController;
    private UserController userController;

    public BibliotekaApplication(BookController bookController, UserController userController) {
        this.bookController = bookController;
        this.userController = userController;
    }

    public static void main(String[] args) {
        SpringApplication.run(BibliotekaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Book book = new Book();
        book.setTitle("ABCD");
        book.setAuthor("John Smith");
        Book book2 = new Book();
        book2.setTitle("DCBA");
        book2.setAuthor("Jane Smith");
        bookController.addNewBook(book);
        bookController.addNewBook(book2);
        User user = new User();
        userController.addNewUser(user);

        bookController.borrowBook(book, user, 10);

        List<Book> allBooks = bookController.getAllBooks();
        allBooks.forEach(System.out::println);

        System.out.println("Avaliable books: " + bookController.getNumberOfAvaliableBooks());
        System.out.println("Borrowed books: " + bookController.getNumberOfBorrowedBooks());
        System.out.println("Fine 1: " + bookController.calculateFine(book.getId()));

        bookController.borrowBook(book2, user, -10);
        System.out.println("Fine 2: " + bookController.calculateFine(book2.getId()));


    }
}
