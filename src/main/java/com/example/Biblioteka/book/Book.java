package com.example.Biblioteka.book;

import com.example.Biblioteka.user.User;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String author;

    @NotNull
    private Boolean borrowed;
    @OneToOne
    private User user;
    @Temporal(TemporalType.TIMESTAMP)
    private Date retDate;

    public boolean isBorrowed() {
        return Boolean.TRUE.equals(borrowed);
    }
}
