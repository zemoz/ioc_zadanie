package com.example.Biblioteka.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FactoryBeanConfig {
    @Bean
    public UserServiceFactory userServiceFactory() {
        return new UserServiceFactory();
    }

    @Bean
    public UserService userService() throws Exception {
        return userServiceFactory().getObject();
    }
}
