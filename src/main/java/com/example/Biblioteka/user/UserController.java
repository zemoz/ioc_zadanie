package com.example.Biblioteka.user;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @PostConstruct
    public void post() {
        System.out.println("JSR 250");
    }

    public void addNewUser(User user) {
        userService.createUser(user);
    }

    public void removeUser(User user) {
        userService.remove(user);
    }
}
