package com.example.Biblioteka.user;

import lombok.Setter;
import org.springframework.stereotype.Service;

public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createUser(User user) {
        userRepository.save(user);
    }

    public void remove(User user) {
        userRepository.delete(user);
    }
}
