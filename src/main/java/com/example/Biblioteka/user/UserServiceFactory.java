package com.example.Biblioteka.user;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceFactory implements FactoryBean<UserService> {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserService getObject() throws Exception {
        return new UserService(userRepository);
    }

    @Override
    public Class<?> getObjectType() {
        return UserService.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
