package com.example.Biblioteka.book;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookControllerTest {
    @Autowired
    private BookController bookController;

    @MockBean
    private BookRepository bookRepository;

    @Test
    public void removeBook() {
        bookController.removeBook(new Book());
        verify(bookRepository, times(1)).delete(any());
    }


    @Test
    public void returnBook() {
        bookController.returnBook(new Book());
        verify(bookRepository, times(1)).save(any());
    }
}